/*
	INSTITUTO POLITECNICO NACIONAL
	ESCUELA SUPERIOR DE COMPUTO

	Simulación de VIH

	Materia:
	Teoría computacional

	Profesor:
	Benjamín Luna Benoso

	Alumno:
	Cortés Piña Oziel

	Grupo:
	2CM5
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include "util.h"

char **yin;
char **yang;
char *output  = 0x0;
size_t width  = 100;
size_t height = 100;
int t = 4;
int n = 1;
int L = 7;
int ra = 1;
int rb = 4;
int doquiet = 0;
int dograp  = 0;
float wait = 0;
float pv = 0.05;
float pr = 0.99;
float pi = 0.00005;

char *grap;
char *grap_head = ".TL\nSimulation HIV\n\
.PP\n\
Square: Healtly cells\n\
.PP\n\
Bullet: Infected A cells\n\
.PP\n\
Delta: Infected B cells\n\
.PP\n\
Times: Dead cells\n\
.G1\n\
frame invis ht 4 wid 6 left solid bot solid\n\
label left \"Cells\"\n\
label bot \"Time in weeks\"\n\
draw h solid square\n\
draw a solid bullet\n\
draw b solid delta\n\
draw d solid times\n";

char *grap_cood = "next h at %i,%i \n\
next a at %i,%i\n\
next b at %i,%i\n\
next d at %i,%i\n";

char **initSpace( size_t h, size_t w ){
	char **s = ecalloc( h, sizeof(char*) );
	for( int i = 0; i < h; i++ )
		s[i] = ecalloc( w, sizeof(char) );
	return s;
}

void countNeigh( char **s, size_t sx, size_t sy, size_t *counta, size_t *countb ){
	if( sx < 0 || sy < 0 || sx >= width || sy >= height ) return;
	for( size_t y = sy-n; y <= sy+n; y++  )
		for( size_t x = sx-n; x <= sx+n; x++)
			if( x >= 0 && x < width  &&
			    y >= 0 && y < height  )
				if( s[y][x] > 0 && s[y][x] < t + 1 ){
					(*counta)++;
				}else if( s[y][x] == t + 1 )
					*(countb)++;
} 

void infectSpace( char **s, float p ){
	for( size_t y=0; y < height; y++ )
		for( size_t x=0; x<width; x++ )
			if( proba(p) )
				s[y][x] = 1;
}

void grapSpace( char **s, size_t time ){
	int grapH = 0,grapA = 0,grapB = 0,grapD = 0;
	for( size_t y=0; y < height; y++ )
		for( size_t x=0; x<width; x++ )
			if( s[y][x] == 0 )
				grapH++;
			else if( s[y][x] > 0 && s[y][x] < t + 1 )
				grapA++;
			else if( s[y][x] == t + 1 )
				grapB++;
			else if( s[y][x] == t + 2 )
				grapD++;

	int len = snprintf( NULL, 0, grap_cood,  time, grapH, time, grapA, time, grapB, time, grapD );
	char *str = ecalloc( len + 1, sizeof(char) );
	sprintf( str, grap_cood, time, grapH, time, grapA, time, grapB, time, grapD );
	rstrcat( &grap, str );
	free(str);
}

void bitmapSpace( char **s, char *file, size_t prefix ){
	size_t row, column, y, x;
	size_t size = width * height * 4; //for 32-bit bitmap only
	char header[54] = { 0 };
	strcpy(header, "BM");
	memset(&header[2],  (int)(54 + size), 1);
	memset(&header[10], (int)54, 1);//always 54
	memset(&header[14], (int)40, 1);//always 40
	memset(&header[18], (int)width, 1);
	memset(&header[22], (int)height, 1);
	memset(&header[26], (short)1, 1);
	memset(&header[28], (short)32, 1);//32bit
	memset(&header[34], (int)size, 1);//pixel size

	unsigned char *pixels = malloc(size);
	for( row = height - 1, y = 0; row >= 0 && y < height; row--, y++ ) {
		for(column = 0, x = 0; column < width && x < width; column++, x++) {
			int p = (row * width + column) * 4;
			if( s[y][x] == 0 ){
				pixels[p + 0] = 255; //blue
				pixels[p + 1] = 0;//green
				pixels[p + 2] = 0;//red
			}else if( s[y][x] > 0 && s[y][x] < t + 1 ){
				pixels[p + 0] = 0;
				pixels[p + 1] = 255;
				pixels[p + 2] = 255;
			}else if( s[y][x] == t + 1 ){
				pixels[p + 0] = 0;
				pixels[p + 1] = 255;
				pixels[p + 2] = 0;
			}else if( s[y][x] == t + 2 ){
				pixels[p + 0] = 0;
				pixels[p + 1] = 0;
				pixels[p + 2] = 255;
			}

		}
	}
	int str_size = snprintf( NULL, 0, "%s-%i.bmp", file, prefix );
	char *str = ecalloc( ++str_size, sizeof(char) );
	sprintf( str, "%s-%i.bmp", file, prefix );
	FILE *fout = efopen(str, "wb");
	fwrite(header, 1, 54, fout);
	fwrite(pixels, 1, size, fout);
	free(str);
	free(pixels);
	fclose(fout);
}

void printSpace( char **s ){
	for( size_t y=0; y < height; y++ )
		for( size_t x=0; x<width; x++ )
			printf("\033[0;%im%i \033[0m%s",
					( s[y][x] == 0   )? 104:
					( s[y][x] <  t+1 )? 103:
					( s[y][x] == t+1 )? 102: 101,
					s[y][x],
					( x == width-1   )?"\n": ""
			);
	printf("\n");
}

void evalSpace( char **yin, char **yang ){
	size_t counta, countb;
	sleep(wait);
	for( size_t y=0; y < height; y++ )
		for( size_t x=0; x<width; x++ ){
			if( yin[y][x] == 0 ){
				counta=0;
				countb=0;
				countNeigh( yin, x, y, &counta, &countb );
				if( counta >= ra || countb >= rb )
					yang[y][x] = 1;
				else
					yang[y][x] = yin[y][x];
			}else if ( yin[y][x]  == t + 2 ){
				if( proba(pr) )
					yang[y][x] = proba(pi)? 1 : 0;
			}else if ( yin[y][x] > 0 && yin[y][x] < t + 2 )
				yang[y][x] = yin[y][x] + 1;
		}
}

int main(int argc, char **argv){
	srand(time(NULL));
	for( int i = 0; i<argc; i++ )
		if(!strcmp(argv[i], "-ra") ) 
			ra = atoi(argv[++i]);
		else if (!strcmp(argv[i], "-q") )
			doquiet = 1;
		else if (!strcmp(argv[i], "-g") )
			dograp = 1;
		else if (!strcmp(argv[i], "-rb") )
			rb = atoi(argv[++i]);
		else if (!strcmp(argv[i], "-L") )
			L  = atoi( argv[++i] );
		else if (!strcmp(argv[i], "-t") )
			t  = atoi( argv[++i] );
		else if (!strcmp(argv[i], "-w") )
			wait = atof( argv[++i] );
		else if (!strcmp(argv[i], "-pr") )
			pr = atof( argv[++i] );
		else if (!strcmp(argv[i], "-pi") )
			pi = atof( argv[++i] );
		else if (!strcmp(argv[i], "-pv") )
			pv = atof( argv[++i] );
		else if (!strcmp(argv[i], "-o") )
			output = argv[++i];
		else if (!strcmp(argv[i], "-s") ){
			width  = atof( strsep( &argv[++i], "x" ) );
			height = atof( argv[i] );
		}
	yin  = initSpace( height, width );
	yang = initSpace( height, width );

	infectSpace( yin, pv );
	if(!doquiet)
		printSpace( yin );

	if(dograp){
		grap = ecalloc( strlen(grap_head) + 1, sizeof(char) );
		strcpy( grap, grap_head );
		grapSpace( yin, 0 );
	}

	for( int i = 0; i < L ; i++ ){
		if( i % 2 ) {
			evalSpace( yang, yin );
			if(dograp)   grapSpace( yin, i );
			if(output)   bitmapSpace( yin, output, i);
			if(!doquiet) printSpace(yin);
		} else {
			evalSpace( yin, yang );
			if(dograp)   grapSpace( yang, i );
			if(output)   bitmapSpace( yang, output, i);
			if(!doquiet) printSpace(yang);
		}
	}

	if(dograp){
		rstrcat( &grap, ".G2\n");
		printf( grap );
		free( grap );
	}
	free(yin);
	free(yang);
}

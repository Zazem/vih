#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>

#define MAX(A, B)               ((A) > (B) ? (A) : (B))
#define MIN(A, B)               ((A) < (B) ? (A) : (B))
#define BETWEEN(X, A, B)        ((A) <= (X) && (X) <= (B))

void *rstrcat( char **dest, char *src);
int proba( float p );
FILE* efopen( char *rute, char *perm );
void die(const char *fmt, ...);
void *ecalloc(size_t nmemb, size_t size);
void *erealloc(void *source, size_t size);

#endif

/* See LICENSE file for copyright and license details. */
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "util.h"

void *rstrcat( char **dest, char *src ){
	*dest = erealloc( *dest, ( strlen(*dest) + strlen(src) + 1) * sizeof(char) );
	strcat( *dest, src );
	return *dest;
}

int proba( float p ){
	return ( rand() / (float) RAND_MAX  )<= p;
}

FILE* efopen( char *rute, char *perm ){
	FILE *file = (!strcmp(rute,"-"))? stdin : fopen( rute, perm );
	if( file == NULL )
		die( "Could not open file %s\n", rute );
	return file;
}

void *
ecalloc(size_t nmemb, size_t size)
{
	void *p;

	if (!(p = calloc(nmemb, size)))
		die("calloc:");
	return p;
}

void *
erealloc(void *source, size_t size)
{
	void *p;

	if (!(p = realloc( source, size )))
		die("realloc:");
	return p;

}

void
die(const char *fmt, ...) {
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	if (fmt[0] && fmt[strlen(fmt)-1] == ':') {
		fputc(' ', stderr);
		perror(NULL);
	} else {
		fputc('\n', stderr);
	}

	exit(1);
}

